# @vlr/tslib-seed-level2
This is a basic seed for typescript library, includes tsconfig, tslint, package.json with all dependencies needed to build, lint and test the library. And gulp files with implementations for these tasks.

# requirements
For all gulp tasks to work, following global packages should be installed: typescript, tslint, nyc, gulp and mocha

# Features
1. Gulp tasks include building, linting, testing and measuring coverage for a typescript library to be created off this seed.
2. Running tests are using old gulp-mocha module so that breakpoint in visual studio would work. So far i have not found a way to do that with latest version of that module.
3. Along with building for latest target (ES2017), there are tasks to build the project for 2 old targets - ES5 and ES6. 
To import from one of those targets, use the corresponding suffix, like the following: import { maxBy } from '@vlr/array-tools/es5'
4. Gitlab CI script will do full test and coverage check on any pull request/merge request.
5. Gitlab CI script also will create version tag on master branch, and publish npm package.
   Version will be taken from npm if it is higher than previous tag, i.e. if it was set manually to higher value, 
   otherwize version from last set tag will be used with increment to patch version.

6. Specifics of level2 - 3 code generators are included into build - @vlr/
type-constructor-gen, @vlr/bucket-gen and @vlr/razor\
   To use those, describe your types via interfaces and put them to files with suffix .type, for example "myType.type.ts"\
   First generator will automatically create "myType.ctor.ts", that will contain constructor function, helping to create objects of that type.\
   Second generator will create index.ts file that will contain following lines:
```
  export * from "./myType.type";
  export * from "./myType.ctor";
```
Also, for purposes of code generation, razor templating engine can be used, it converts .rzr templates into generators.

# Creating a typescript library from this seed
First, in your 'users/{username}/.ssh' folder, create files 'gitlab' and 'npm' with access tokens allowing to access gitlab api and publish npm packages respectively
Then, in folder of cloned tslib-seed-level2 run "gulp startNewLib -n {gitlab_login}/{repository_name}"
This creates a repository on gitlab populated from this seed and feature/first-implementation branch


However, if you feel like doing all that manually, here are the steps:
1. create a repository on gitlab
2. somewhere in your local folder, clone this repository
3. set "origin" remote to your new gitlab repository
4. push master branch to your repository

5. In new repository settings, go to Settings -> General -> MergeRequests, set fast-forward and "only allow" settings
6. Settings -> CI/CD -> Environment Variables, set NPM_AUTH_TOKEN and GIT_PRIVATE_KEY
7. Settings -> Repository -> Protected Branches, set noone to be able to push to master 

8. create a branch for first version of your library.
9. in that branch update gitlab-ci.yml to have correct address of your gitlab repository
10. update your package.json and readme files