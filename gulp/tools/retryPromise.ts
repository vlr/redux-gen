export async function retryPromise(func: () => Promise<void>, retryCount: number = 5): Promise<void> {
  let count = retryCount;
  let error;
  while (count) {
    try {
      const result = await func();
      return result;
    } catch (err) {
      count--;
      error = err;
    }
  }
  throw error;

}

