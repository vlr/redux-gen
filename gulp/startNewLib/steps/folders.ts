import { projectName } from "./projectName";

export async function goOneDirUp(): Promise<void> {
  process.chdir("..");
}

export async function goIntoLibDir(): Promise<void> {
  process.chdir(projectName());
}
