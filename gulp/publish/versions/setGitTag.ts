import { Version } from "./version";
import * as gitTag from "git-tag";

export function setGitTag(version: string): Promise<void> {
  console.log(version);
  return new Promise(resolve => gitTag({ localOnly: true }).create(version, version, resolve));
}

export function printVersion(version: Version): string {
  return `${version.major}.${version.minor}.${version.patch}`;
}
